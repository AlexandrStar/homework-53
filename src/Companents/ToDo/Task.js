import React from 'react';
import './ToDo.css';

const Task = (props) =>  {

        return (
            props.entries.map((task) => {
                return (
                    <div className='task' key={task.id}>
                        <p>{task.text}</p>
                        <button onClick={() => props.remove(task.id)} className='btn-remove'>Remove</button>
                    </div>
                )
            })
        )

};

export default Task;