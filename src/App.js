import React, { Component } from 'react';
import './App.css';
import AddTaskForm from './Companents/ToDo/AddTaskForm';
import Task from "./Companents/ToDo/Task";

class App extends Component {

    state = {
        tasks: [
            {text: 'Купить молока', id: 1},
            {text: 'Купить подгузники', id: 2},
            {text: 'Купить хлеб', id: 3}
        ],
        textInput: ''
    };

    addTask = () => {
        if (this.state.textInput !== '') {
            const tasks =  [...this.state.tasks];
            const newTask = {
                text: this.state.textInput,
                id: Date.now()
            };

            tasks.push(newTask);

            this.setState({
                tasks: tasks
            });
        }
    };

    removeItems = id => {
        const tasks = [...this.state.tasks];
        const index = tasks.findIndex(task => {
            return (
                task.id === id
            )
        });
        tasks.splice(index, 1);
        this.setState({tasks})
    };

    changeHandler = (event) => {
        this.setState({
            textInput: event.target.value
        })
    };

    render() {
        return (
            <div className="App">
                <AddTaskForm change={(event) => this.changeHandler(event)} add={() => this.addTask()}/>
                <Task entries = {this.state.tasks}
                      remove = {(id) => this.removeItems(id)}
                />
            </div>
        );
    }
}

export default App;
